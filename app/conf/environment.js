 module.exports = {

  capabilities: 
  {
    browserName: 'chrome', 
    version: '',
    platform: 'ANY',
    chromeOptions: 
    {
// in this case I simulate a mobile device with language in es-ES. You can change at your own needs.
      args: ['--user-agent=Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5', 'lang=es-ES'] ,
          prefs: 
          {
            intl: { accept_languages: "es-ES" },
          },
    },    

  },

   baseUrl: 'url to testing'
};
