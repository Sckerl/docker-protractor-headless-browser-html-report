#!/bin/bash

# Ex 1:
# $ export TEST_IMG_NAME=test1
# $ ./build.sh

# Ex 2:
# $ TEST_IMG_NAME=test1 ./build.sh


if [ -z "$TEST_IMG_NAME" ]; then
	echo "TEST_IMG_NAME is not defined!"
	exit 1
fi

docker build -t $TEST_IMG_NAME .
