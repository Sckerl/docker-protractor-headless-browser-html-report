var LoginMod = function(){
    

	this.login = function()
	{
		// this is an example. Addapt to the login site
		browser.driver.findElement(by.id('username')).sendKeys(browser.params.loginData.user); 
		browser.driver.findElement(by.id('password')).sendKeys(browser.params.loginData.pass);
		browser.driver.findElement(by.id('button')).click();	
	};


	this.logout = function(){

		browser.actions().click(element(/* an element */ )).perform();
		browser.sleep(3000);
		// checking that logout page it shows
		expect(element(/* an element */).isPresent()).toBe(true);
		browser.sleep(2000);
	  };	  
};

module.exports = LoginMod;
