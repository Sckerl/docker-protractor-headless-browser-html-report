var path = require('path');
var env = require('./environment.js');
var HtmlReporter = require('protractor-html-screenshot-reporter');
var fs = require('fs');


//timestamp
function basePathBuilder() {
  var currentDate = new Date(),
  totalDateString = currentDate.getDate()+'-'+ (currentDate.getMonth()+1)+'-'+(currentDate.getFullYear().toString().substr(2,2))+'-'+currentDate.getHours()+'h'+currentDate.getMinutes()+'m'+currentDate.getSeconds()+'s';
  return totalDateString
};
var timeStamp = basePathBuilder();
var directory = __dirname;


//name of the environement
var urlEnv = env.baseUrl.replace("https://","").replace(":","-").replace("/","-");

var path_report = '/app/results/' + urlEnv + '/' + env.capabilities.browserName + '/' + urlEnv + '-' + timeStamp;
//html reporter
var reporter = new HtmlReporter({
  baseDirectory: path_report,
  preserveDirectory: true,
  pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
      return path.join(capabilities.caps_.browser+descriptions.join('-')+capabilities.caps_.platform);
  },
  docTitle: 'Title of Report',
  docHeader: 'Title of Body Report',
  docName: 'index.html',
  cssOverrideFile: '/app/node_modules/protractor-html-screenshot-reporter/lib/estilo.css',
  takeScreenShotsOnlyForFailedSpecs: false
});


exports.config = {

//  seleniumAddress: 'http://localhost:60951/wd/hub',
  chromeDriver: '/app/node_modules/protractor/selenium/chromedriver',
//  seleniumServerJar: '/app/node_modules/protractor/selenium/selenium-server-standalone-2.45.0.jar',
//  seleniumArgs is for .JAR only   
//	seleniumArgs: ['--browserTimeout=60', '--debug', '--log=chromelog.txt'],

  //seleniumAddress: env.seleniumAddress,
  capabilities: env.capabilities,

  baseUrl: env.baseUrl,

  params: {
  //Username and Password for some login page you'll need
  // to execute line command: ./protractor <path to conf.js>/conf.js --params.loginData.user 'xxxxx@yyyyy.com'
    loginData : {
      user : "username",
      pass : "password123"
    },
  },


  jasmineNodeOpts: {
    onComplete: null,
    isVerbose: true,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 1360000
  },

   specs: ['./test-suite.js'],

  onPrepare: function() 
  {
    jasmine.getEnv().addReporter(reporter);
    beforeEach(function () {
    browser.ignoreSynchronization = false;
  });
  

//Some functions
  //Vars
    VARS = {

    selectDropdownByNumber : function (element, index, milliseconds) {
          element.findElements(by.tagName('option'))
            .then(function(options) {
              options[index].click();
            });
          if (typeof milliseconds !== 'undefined') {
            browser.sleep(milliseconds);
         }
      },


    //dropdown selector
    selectDropdownbyNum : function ( element, optionNum ) {
      if (optionNum){
        var options = element.all(by.tagName('option'))
        .then(function(options){
          options[optionNum].click();
        });
      }
    },
    /**
    * Usage: Generate random string.
    * characterLength :  Length of string.
    * Returns : Random string.
    */
    getRandomString : function (characterLength) {
      var randomText = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < characterLength; i++)
        randomText += possible.charAt(Math.floor(Math.random() * possible.length));
      return randomText;
    },
  }
},
};
