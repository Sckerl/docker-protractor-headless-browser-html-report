#!/bin/bash

# Ex 1:
# $ export TEST_IMG_NAME=test1
# $ ./launch.sh

# Ex 2:
# $ TEST_IMG_NAME=test1 ./launch.sh

if [ -z "$TEST_IMG_NAME" ]; then
	echo "TEST_IMG_NAME is not defined!"
	exit 1
fi

echo "Starting...."
docker run -it --rm --net=host \
	-v /dev/shm:/dev/shm \
	-v $(pwd)/app/conf:/app/conf \
	-v $(pwd)/app/results:/app/results \
	$TEST_IMG_NAME

echo "# Change permission on Results folder to own user...."
sudo chown -R $(whoami):$(id -g -n $(whoami)) app/results