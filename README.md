# README #

Project by Mariano Sckerl and Eduardo Cuomo.

### Install Docker ###

* [Docker Install Page](https://docs.docker.com/engine/installation/)

### Download Docker Project ###

* git clone git@bitbucket.org:Sckerl/docker-protractor-headless-browser-html-report.git in a empty folder.

### How to build Dockerfile ###

* On the folder where project is downloaded, run the command:

```
$ export TEST_IMG_NAME=name_of_container (!!REMEMBER name_of_container NAME)
$ ./build.sh
```

Docker will start to download all the dependencies. Be pacient.

* Important!! If you need to make changes in css style sheet or report file, you need to re build the docker image for the changes to take effect.

### Prepare the environment ###

* To run your test, you need the next tree structure:
```    
/Root_folder_test
   | 
   |_launch.sh
   |
   |_app/
        |_run.sh
        |
        |_conf/
        |    |_conf.js
        |    |_environment.js
        |    |_loginmod.js
        |    |_test-suite.js
	|    |_estilo.css
	|    |_jsonparser.js
        |
        |_results/
```

* You only need to edit files inside the "conf" folder

* conf.js is the main configuration file (Don't change the file name)
* environment.js is the setting file for some variables (url, browser)
* loginmod.js is an example login module for login page (it can be reuse and customize)
* test-suite.js is the testcase script file.

* ./build.sh - ./Dockerfile - ./app/package.json
    These files is only used to build docker image. (once)

### Before to launch ###

* Before to launch (execute the service) maybe you want to customize the output html report.
* To setup the report you can edit "app/conf/estilo.css" to personalize the style of "app/conf/jsonparser.js" file. I put an example, "app/conf/example-template.html" file to help you to take a quick preview of the html report style (estilo.css) ;P
* In "estilo.css" you can define a logo header. I recomended a picture weblink.

### Launch the test ###

* export TEST_IMG_NAME=name_of_container (NAME WHEN CREATED IT)
* If you forget the docker image name then run:
```
$ docker images
```
* In Root_folder_test run:
```
$ ./launch.sh
```

* Protractor will start executing the "test-suite.js" file with the config from (conf.js + environment.js + loginmod.js) + (estilo.css + jsonparser.js)

* There is a lot of information about to write test case

### HTML Report ###

* Protractor generate a report html (index.html by default) with style file "estilo.css" associate to him.
* The report path is located in results/url_base/browser_name/url_base+timestamp. This way allow you to run a several tests without overwriting the previous.

And that is all!! I hope it's useful 4U.
