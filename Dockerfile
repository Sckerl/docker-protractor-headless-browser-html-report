FROM node:slim

WORKDIR /tmp
	RUN apt-get update
	RUN apt-get install -y xvfb wget openjdk-7-jre git
	RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	RUN dpkg --unpack google-chrome-stable_current_amd64.deb
	RUN apt-get install -f -y
	RUN apt-get clean
	RUN rm google-chrome-stable_current_amd64.deb
	RUN mkdir /app

WORKDIR /app

ADD app/package.json /app
ADD app/run.sh /app

RUN npm install
RUN ./node_modules/protractor/bin/webdriver-manager update

RUN ln -fs /app/conf/estilo.css /app/node_modules/protractor-html-screenshot-reporter/lib/estilo.css

RUN ln -fs /app/conf/jsonparser.js /app/node_modules/protractor-html-screenshot-reporter/lib/jsonparser.js

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp

# Set default image container command to launch linting, xvfb, integration tests
ENTRYPOINT ["/app/run.sh"]
